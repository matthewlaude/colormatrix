package com.example.colormatrix_dynamiclists.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colormatrix_dynamiclists.model.ColorRepo
import kotlinx.coroutines.launch

class ColorViewModel : ViewModel() {

    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<List<Int>>()
    val randomColors : LiveData<List<Int>> get() = _randomColor

    fun getRandomColor(repeater: Int) {
        viewModelScope.launch {
            val randomColor = repo.getRandomColor(repeater)
            _randomColor.value = randomColor
        }
    }
}