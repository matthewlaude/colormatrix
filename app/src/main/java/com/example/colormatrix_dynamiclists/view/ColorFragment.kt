package com.example.colormatrix_dynamiclists.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.colormatrix_dynamiclists.R
import com.example.colormatrix_dynamiclists.databinding.FragmentColorBinding
import com.example.colormatrix_dynamiclists.viewmodel.ColorViewModel

class ColorFragment : Fragment(), ColorsAdapter.Listener {

    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!
    private val colorViewModel by viewModels<ColorViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentColorBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvList.layoutManager = GridLayoutManager(context, 3)

        binding.btnFetchColors.setOnClickListener {
            binding.rvList.adapter = ColorsAdapter(this).apply {
                val repeater = binding.etInt.text.toString().toInt()
                colorViewModel.getRandomColor(repeater)

                colorViewModel.randomColors.observe(viewLifecycleOwner) {
                    addColors(it)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun itemClicked(num: Int) {
        println(num.toString().split(""))
        findNavController().navigate(R.id.action_colorFragment_to_numberFragment)
    }

}