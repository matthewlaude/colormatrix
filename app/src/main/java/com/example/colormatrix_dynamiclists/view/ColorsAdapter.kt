package com.example.colormatrix_dynamiclists.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.colormatrix_dynamiclists.databinding.ItemColorBinding

class ColorsAdapter(
    private val listener: Listener
) : RecyclerView.Adapter<ColorsAdapter.ColorsViewHolder>() {

    private var colors = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorsViewHolder {
        val binding = ItemColorBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ColorsViewHolder(binding).apply {
            binding.ivColor.setOnClickListener {
                val item = colors[adapterPosition]
                listener.itemClicked(item)
            }
        }
    }

    override fun onBindViewHolder(holder: ColorsViewHolder, position: Int) {
        val color = colors[position]
        holder.loadColor(color)
    }

    override fun getItemCount(): Int {
        return colors.size
    }

    fun addColors(colors: List<Int>) {
        this.colors = colors.toMutableList()
        notifyDataSetChanged()
    }

    class ColorsViewHolder(
        private val binding: ItemColorBinding
        ) : RecyclerView.ViewHolder(binding.root) {

        fun loadColor(color: Int) {
            binding.ivColor.setBackgroundColor(color)
        }
    }

    interface Listener {
        fun itemClicked(num: Int)
    }
}