package com.example.colormatrix_dynamiclists

import android.graphics.Color
import kotlin.random.Random

val randomColor
    get() = Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))