package com.example.colormatrix_dynamiclists.model.remote

interface ColorApi {

    suspend fun getRandomColor(repeater: Int): List<Int>
}