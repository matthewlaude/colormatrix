package com.example.colormatrix_dynamiclists.model

import com.example.colormatrix_dynamiclists.model.remote.ColorApi
import com.example.colormatrix_dynamiclists.randomColor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object ColorRepo {

    private val colorApi = object : ColorApi {
        override suspend fun getRandomColor(repeater: Int): List<Int> {
            var fetchedColors: MutableList<Int> = mutableListOf()

            repeat(repeater) {
                fetchedColors.add(randomColor)
            }
            return fetchedColors
        }
    }

    suspend fun getRandomColor(repeater: Int): List<Int> = withContext(Dispatchers.IO) {
        colorApi.getRandomColor(repeater)
    }
}